---
layout: page
title: Home
id: home
permalink: /
---

# 🪴 Welcome!

<p style="padding: 3em 1em; background: #f5f7ff; border-radius: 4px;">
  Take a look at <span style="font-weight: bold">[[Digital Garden|my note on what a Digital Garden is]]</span> to get started on your exploration.
</p>

Below is a graph of all the notes in this Digital Garden, and the connections between them. Currently, it's not displaying quite right. Like most things here it's a work in progress.

{% include notes_graph.html %}

<style>
  .wrapper {
    max-width: 46em;
  }
</style>
