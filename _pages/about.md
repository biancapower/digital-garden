---
layout: page
title: About
permalink: /about
---

To learn about the purpose of this site see [[digital garden|What is a Digital Garden?]]

To learn more about what matters to me see [[my values]].
