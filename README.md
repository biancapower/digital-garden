# My Digital Garden

Living and growing [here](https://biancapower.gitlab.io/digital-garden/).

This is my Digital Garden. It's a place where I write rough, unpolished, Work in Progress... stuff. Like a traditional garden, a Digital Garden is ever-evolving.

To learn more about digital gardens and their purpose check out [this explanation by Joel Hooks](https://joelhooks.com/digital-garden).

### Acknowledgement
The basic structure of this site was forked from Maxime Vaillancourt's [Digital garden Jekyll template](https://github.com/maximevaillancourt/digital-garden-jekyll-template).