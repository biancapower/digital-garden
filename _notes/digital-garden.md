---
title: What is a digital garden?
---

This is my Digital Garden. It's a place where I write rough, unpolished, Work in Progress... stuff. Like a traditional garden, a Digital Garden is ever-evolving.

Eventually I will flesh out my own definition, but for now, to learn more about digital gardens and their purpose check out [this explanation by Joel Hooks](https://joelhooks.com/digital-garden).