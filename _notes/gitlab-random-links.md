---
title: GitLab Interesting Links
---
## Key Links
### GitLab Handbook
- [Values](https://about.gitlab.com/handbook/values/)
- [Leadership](https://about.gitlab.com/handbook/leadership/)
- [Communication](https://about.gitlab.com/handbook/communication/)
- [Hiring](https://about.gitlab.com/handbook/hiring/)
- [Jobs FAQ](https://about.gitlab.com/jobs/faq/)

## Written

- [ ] [Interviewing at GitLab](https://about.gitlab.com/handbook/hiring/interviewing/#what-to-expect-during-an-interview-with-a-recruiter)

- [ ] [Jobs at GitLab FAQ](https://about.gitlab.com/jobs/faq/#whats-it-like-to-work-at-gitlab)

- [ ] [GitLab Inc. takes The DevOps Platform public](https://about.gitlab.com/blog/2021/10/14/gitlab-inc-takes-the-devops-platform-public/)

- [ ] [GitLab's All Remote Playbook](https://about.gitlab.com/company/culture/all-remote/)

- [ ] [The complete guide to starting a remote job](https://about.gitlab.com/company/culture/all-remote/getting-started/)

- [ ] [GitLab Learn](https://about.gitlab.com/learn/)

## Video

- [ ] [GitLab Playlists](https://www.youtube.com/c/Gitlab/playlists)

	- [ ] Remote by GitLab 2021 (Playlists)
		- [ ] [Main Stage](https://youtube.com/playlist?list=PLFGfElNsQthbwQbc1PbKGc_tzQqjAIT1x)
		- [ ] [Elevating Culture](https://youtube.com/playlist?list=PLFGfElNsQthbm6mFYcVITVwOTF5onrMIF)
		- [ ] [Making Remote Work](https://youtube.com/playlist?list=PLFGfElNsQthb4yVAwGzoEq39qs6x2EbCd)
	- [ ] [Remote Work Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc)
	- [ ] [Group Conversations](https://youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e)
	- [Iteration Office Hours](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=%22Iteration+Office+Hours%22)

- [ ] [GitLab 2018 Cape Town Summit Keynote](https://youtu.be/4BIsON95fl8)

- [ ] [Commit San Francisco 2020: Opening Keynote - The Power of GitLab](https://youtu.be/tIm643kyQqs)

- [x] [Y Combinator interview training with GitLab CEO and Humand Technologies](https://youtu.be/DtsnrkByPnw)

- [ ] [12 things that aren't iteration?
](https://youtu.be/BW6TWwNZzIQ)

## Courses

- [ ] [GitLab Values Badge](https://gitlab.edcast.com/pathways/ECL-19a443fa-73d2-4084-92a3-4ad84fd57989)
- [ ] [Coursera "How to Manage a Remote Team" by GitLab](https://www.coursera.org/learn/remote-team-management)
- [ ] [Public GitLab Certifications](https://about.gitlab.com/learn/certifications/public/)

## Awesome Examples
- [Great example of a GitLab README profile](https://gitlab.com/kencjohnston)
- [Example "taking meaningful time off" management issue](https://gitlab.com/gitlab-com/Product/-/issues/4059)
- [Example use of mermaid with colours](https://about.gitlab.com/handbook/communication/#posting-in-company-fyi)

## Other
- [GitLab current Technical Writing Team](https://about.gitlab.com/company/team/?department=technical-writing)
- [Learning & Development People Group](https://about.gitlab.com/handbook/people-group/learning-and-development/)

- [Companies inspired by GitLab](https://about.gitlab.com/handbook/inspired-by-gitlab/)

- [Onboarding Issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding.md#all-gitlabbers)