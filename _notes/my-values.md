---
title: My values
---

## Transparency

- Unless it risks hurting someone to do so, default to being open and transparent in all things

## Positive assumptions

- Always assume positive intent

- Always remember that everyone is fighting a secret battle I know nothing about

## Always learning
- Everything is an opportunity to learn