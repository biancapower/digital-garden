---
title: GitLab Values
---

This and several other GitLab pages form part of my process of exploring GitLab's Handbook.

[GitLab's six core values](https://about.gitlab.com/handbook/values/) spell **CREDIT**:

**🤝 C**ollaboration

**📈 R**esults

**⏱️ E**fficiency

**🌐 D**iversity, Inclusion & Belonging

**👣 I**teration

**👁️ T**ransparency

---

GitLab's values really resonate with me, and the six core values are easy to remember - however keeping track of the sub-values felt overwhelming to me when reading through the [GitLab values page](https://about.gitlab.com/handbook/values/). While all of the sub-values completely make sense to me, there are a lot, and a lot of cross-links between sub- and core- values. Here, I want to try to represent that visually to "get my head around it" a bit more easily.


## 🤝 Collaboration

> We demonstrate collaboration when we take action to help others and include other's (both internal and external) input (both help and feedback) to achieve the best possible outcome.

- Kindness
- Share
- Negative feedback is 1-1
- Provide feedback in a timely manner
- Say thanks
- Give feedback effectively
- Get to know each other
- Reach across company departments
- Don't pull rank
- **Assume positive intent**
- Address behavior, but don't label people
- Say sorry
- No ego
- See others succeed
- Don't let each other fail
- People are not their work
- Do it yourself
- Blameless problem solving
- Short toes
- It's impossible to know everything
- Collaboration is not consensus
- Collaboration is not playing politics


## 📈 Results

> We demonstrate results when we do what we promised to each other, customers, users, and investors.

- Measure results not hours
- Dogfooding
- Customer results
- Give agency
- Write promises down
- Growth mindset
- Global optimization
- Tenacity
- Ownership
- Sense of urgency
- Ambitious
- Perseverance
- Bias for action
- Disagree, commit, and disagree
- Accepting uncertainty
- Escalate to unblock


## ⏱️ Efficiency

> We demonstrate efficiency when we work on the right things, not doing more than needed, and not duplicating work.

- Only Healthy Constraints
- Write things down
- Boring solutions
- **Self-service and self-learning**
- Efficiency for the right group
- Be respectful of others' time
- Spend company money like it's your own
- Frugality
- ConvDev
- Short verbal answers
- Keep broadcasts short
- **Managers of one**
- Freedom and responsibility over rigidity
- Accept mistakes
- **Move fast by shipping the minimal viable change**
- Embrace change


## 🌐 Diversity, Inclusion & Belonging

> We demonstrate diversity, inclusion and belongings when we foster an environment where everyone can thrive and ensuring that GitLab is a place where people from every background and circumstance feel like they belong and can contribute.

- **Bias towards asynchronous communication**
- Embracing uncomfortable ideas and conversations
- Understanding the impact of microaggressions
- Seek diverse perspectives
- Make family feel welcome
- Shift working hours for a cause
- Be a mentor
- Culture fit is a bad excuse
- Religion and politics at work
- Quirkiness
- Building a safe community
- Unconscious bias
- Inclusive benefits
- Inclusive language & pronouns
- Inclusive interviewing
- Inclusive meetings
- Inclusive and fair policy to regions with fewer employees
- See Something, Say Something
- Embracing Neurodiversity
- Family and friends first, work second


## 👣 Iteration

> We demonstrate iteration when we do the smallest thing possible, getting it out quickly for feedback and making changes based that feedback.

- **Don't wait**
- Set a due date
- Cleanup over sign-off
- Start off by impacting the fewest users possible
- Reduce cycle time
- Work as part of the community
- **Minimal Viable Change (MVC)**
- Make a proposal
- Everything is in draft
- Under construction
- Low level of shame
- Cultural lens
- Focus on improvement
- Be deliberate about scale
- Resist bundling
- Make two-way door decisions
- Changing proposals isn't iteration
- Embracing Iteration
- Make small merge requests
- Always iterate deliberately
- See it in action
- Examples of iteration in other companies
- 12 things that are not iteration


## 👁️ Transparency

> We demonstrate transparency when we are open with as many things as possible reducing the threshold to contribution and make collaboration easier.

- Public by default
- Not public
- Directness
- Articulate when you change your mind
- Surface issues constructively
- Transparency is most valuable if you continue to do it when there are costs
- **Single Source of Truth**
- Findability
- **Say why, not just what**
- Reproducibility