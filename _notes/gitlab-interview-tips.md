---
title: GitLab Interview Tips
---

## Interview 2

> talk through the approach you took in the examples of work you shared as part of your application, your experience up to now, understand what you're looking for in a technical writing position, and answer any questions you have.

---
From [GitLab's Handbook](https://about.gitlab.com/handbook/hiring/interviewing/):

## [What to Expect During an Interview with a Recruiter](https://about.gitlab.com/handbook/hiring/interviewing/#what-to-expect-during-an-interview-with-a-recruiter)

1. Why are you interested in GitLab?
1. What are you looking for in your next position?
1. Why did you join and leave your last three positions?
1. What is your experience with X? (for each of the skills listed in the position description)
1. [STAR Method](https://www.themuse.com/advice/star-interview-method) questions and simple technical or skills-related questions


### No need to prepare

1. What is your current location and do you have any plans to relocate? (relevant in context of compensation, country-hiring guidelines, and in case an offer would be made)
1. Do you require visa sponsorship or a work permit to work for GitLab? Or do you require the work permit to be transferred to GitLab? If the answer is yes, we will not be able to proceed, unless you currently hold a work permit in the Netherlands. You can refer to this page for further clarity
1. What is the notice period you would need if you were hired?
1. Is this salary range in line with your expectations? At GitLab, we are committed to paying competitively and equitably. Therefore, we set our offers based on market pay rather than a candidate's pay history. We walk through the Compensation Calculator with each candidate so that we can address any gaps in expectations early on. Only candidates who are in the screening stage or later can access our compensation calculator by following applicants sign up.
1. Do you know how GitLab will employ you in the country you’re based? (As GitLab keeps growing rapidly it is important to share how we can employ people in each country. We should redirect the candidates to the Contracts and International Expansion page and go through it with them.)