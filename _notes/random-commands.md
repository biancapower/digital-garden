---
title: Useful Commands
---

## Vim

- `g/^$\|^[^#]/d` remove all lines in a file that are either blank or don't start with `#`
	- used to create a TOC