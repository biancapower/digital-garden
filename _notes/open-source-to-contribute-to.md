---
title: Open Source Repos
---

- [The Linux Foundation Projects](https://linuxfoundation.org/projects/)

- [Homebrew (good suggestion in README)](https://github.com/Homebrew/brew)
- [git cola - a git GUI](https://github.com/git-cola/git-cola)
- [https://stackstorm.com/](https://stackstorm.com/)
- [oh my zsh](https://github.com/ohmyzsh/ohmyzsh/wiki/Volunteers)
- [Jekyll](https://github.com/jekyll/jekyll)
	- also, [contribute a tutorial](https://jekyllrb.com/tutorials/home/)
- [TinyMCE](https://github.com/tinymce/tinymce)
- [Forem](https://github.com/forem)