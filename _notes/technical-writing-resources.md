---
title: Technical Writing Resources
---

- [A List of Open Source Projects with Volunteer Documentation Opportunities](https://www.reddit.com/r/technicalwriting/comments/800a9a/a_list_of_open_source_projects_with_volunteer/)

- [Join #Hacktoberfest and contribute as a technical writer](https://javascript.plainenglish.io/how-to-contribute-to-open-source-as-a-technical-writer-bb708245480c)

- [4 tips to becoming a technical writer with open source contributions](https://opensource.com/article/21/11/technical-writing-open-source)

- [Google's Season of Docs](https://developers.google.com/season-of-docs/docs/tech-writer-guide)