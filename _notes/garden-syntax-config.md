---
title: Syntax & Config for my Digital Garden
---

This is my summary of the information documented [here](https://github.com/maximevaillancourt/digital-garden-jekyll-template/blob/master/_notes/your-first-note.md#link-syntax)
### Link Syntax

Let's say we have a file in the `_notes` folder named `digital-garden.md`, with the title (at the top of the file) `title: Introduction to Digital Gardens`. There are four ways we can link to this file using the double-bracket notation:

- Using the note title: [[What is a Digital Garden?]]
	- [`[What is a Digital Garden?]`]
- Using the note's filename: [[digital garden]]
	- [`[digital garden]`]
- Using the note's title, with a label: [[what is a digital garden?|this link uses the note's title with a label]]
	- [`[what is a digital garden?|this link uses the note's title with a label]`]
- Using the note's filename, with a label: [[Digital Garden|this link uses the note's filename with a label]]
	- [`[Digital Garden|this link uses the note's filename with a label]`]

# Let's see what headings look like
This is a `#` Heading

## Level 2
This is a `##` Heading

### Level 3
This is a `###` Heading

#### Level 4
This is a `####` Heading

##### Level 5
This is a `#####` Heading

###### Level 6
This is a `######` Heading