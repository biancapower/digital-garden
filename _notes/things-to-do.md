---
title: Things to Do
---

## For Fun

- [ ] Play [Twilioquest](https://www.twilio.com/quest)

## Tech Writer / UX

- [ ] [Figma UI UX Design Essentials Course](https://www.skillshare.com/classes/Figma-UI-UX-Design-Essentials/1088693386/projects)
- [ ] Figure out my "elevator pitch" of how my background in education fits with technical writing

### Regular sources of info

- [LinkedIn Technical Writer Forum](https://www.linkedin.com/groups/112571/)
- Write the Docs Slack

### Things to Google
- [ ] Books on Technical Writing
