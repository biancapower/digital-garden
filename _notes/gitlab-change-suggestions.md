---
title: GitLab Change Suggestions
---

- [ ] [DRI should link to definition in the first instance on this page](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree)
	- [Point 36 of the Writing Style Guidelines](https://about.gitlab.com/handbook/communication/#writing-style-guidelines)

- [ ] [Final paragraph in this section has clunky grammar](https://about.gitlab.com/handbook/values/#understanding-the-impact-of-microaggressions)

- [ ] [Links broken in final paragraph](https://about.gitlab.com/handbook/values/#religion-and-politics-at-work)

- [ ] Use of he / she and similar -> they/them

- [ ] [Typo "Never call a revision an iteration because it almost the opposite."](https://about.gitlab.com/handbook/values/#changing-proposals-isnt-iteration)

- [x] [Says "soup" instead of "stew"](https://about.gitlab.com/handbook/values/#how-to-scale-the-business-while-preserving-gitlab-values)
	- [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/102285)

- [x] [espeically <- typo](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/communication/index.html.md)
	- [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/102284)

- [ ] [typo in "to scroll back review points of interest"](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/communication/index.html.md)

- [x] [typo in "guide on GitLab meeting b est practices"](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/communication/index.html.md)
	- [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/102281)

- [x] [typo "when naming the month it is clear that the number of the day of the month"](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/communication/index.html.md)
	- [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/102283)