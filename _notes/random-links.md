---
title: Random links to check out later
---

- [https://www.disciplemedia.com/](https://www.disciplemedia.com/)

- [https://gumroad.com/](https://gumroad.com/)

- Ben Horowitz
	- [Ben Horowitz’s Best Startup Advice](https://blog.producthunt.com/ben-horowitz-s-best-startup-advice-7e8c09c8de1b)
	- [Book: The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers](https://www.amazon.com/The-Hard-Thing-About-Things/dp/0062273205)
	- [Controversial commencement speech - Don't follow your goals, it's about what you can contribute](https://youtu.be/WRYRBGX4lVM)
	- [Product Hunt chat](https://www.producthunt.com/live/ben-horowitz)

- [https://nohq.co/](https://nohq.co/)

- [How The First 15 Minutes Of Amazon’s Leadership Meetings Spark Great Ideas And Better Conversations](https://www.forbes.com/sites/carminegallo/2019/06/18/how-the-first-15-minutes-of-amazons-leadership-meetings-sparks-great-ideas-and-better-conversations/?sh=6aa79b6054ca)

- [How to avoid Death by Powerpoint](https://youtu.be/Iwpi1Lm6dFo)