---
title: GitLab Things to Do
---

- [ ] [Figma GitLab Plugin](https://www.figma.com/community/plugin/860845891704482356/GitLab)
- [ ] Set up GitLab Workflow VS Code Extension
- [ ] GitLab Dark Mode!
- [ ] View links in description of [latest GitLab release](https://youtu.be/ZIfuTTvTMFs)
- [ ] Look through [Technical Writing Project](https://gitlab.com/gitlab-org/technical-writing) issues etc
- [ ] Read the docs on [GitLab personas](https://about.gitlab.com/handbook/product/personas/#list-of-user-personas)
- [ ] Get the docs site running locally
	- [ ] with linters
	- [ ] look through metadata e.g. at tech writer assignments
- [ ] Explore the docs site
- [ ] Look at what the GitLab Technical Writing Team are currently working on
	- [ ] incl blog posts

### Questions I have
- [ ] [Pajamas](https://design.gitlab.com/) - what is it?
- [ ] I've read / heard that "Teams" at GitLab are made up of 8-10 engineers, one PM and one designer. How does the Technical Writer fit in? Do they work across multiple teams?
	- A: on the [Technical Writing](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments) page of the Handbook

- [ ] The [Interesting things UX is working on - February 2021](https://about.gitlab.com/blog/2021/02/12/interesting-things-ux-is-working-on-february-2021/) article mentions [Help users configure API fuzzing scanners more efficiently](https://about.gitlab.com/blog/2021/02/12/interesting-things-ux-is-working-on-february-2021/#help-users-configure-api-fuzzing-scanners-more-efficiently). I was looking through [the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/299234) for this feature, and looking at the Figma file and the Loom video walkthrough, which is really cool.
	- Question: Do Technical Writers ever get brought in to give feedback on the Figma file, around microcopy for example?
		- it seems like in this case the `Technical Writing` label wasn't applied until much later.
